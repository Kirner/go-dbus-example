package main

import (
	"fmt"
	"os"

	"github.com/godbus/dbus"
	"github.com/godbus/dbus/introspect"
)

const dbus_service_name = "com.gitlab.Kirner"
const dbus_interface = "com.gitlab.Kirner.GoExample"
const dbus_path = "/com/gitlab/kirner/goexample"

const interface_xml = `
<node>
	<interface name="` + dbus_interface + `">
		<method name="Example">
			<arg direction="out" type="s"/>
		</method>
	</interface>` + introspect.IntrospectDataString + `</node> `

type DbusObject string

func (f DbusObject) Example() (string, *dbus.Error) {
	fmt.Println(f)
	return string(f), nil
}

func main() {
	conn, err := dbus.ConnectSessionBus()
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	dbus_object := DbusObject("You called the function!")
	conn.Export(dbus_object, dbus_path, dbus_interface)
	conn.Export(introspect.Introspectable(interface_xml), dbus_path,
		"org.freedesktop.DBus.Introspectable")

	reply, err := conn.RequestName(dbus_service_name, dbus.NameFlagDoNotQueue)
	if err != nil {
		panic(err)
	}
	if reply != dbus.RequestNameReplyPrimaryOwner {
		fmt.Fprintln(os.Stderr, "name already taken")
		os.Exit(1)
	}

	fmt.Println("Service Started and listening on: \"" + dbus_interface + "\"")
	select {}
}
